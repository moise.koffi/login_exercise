const mongoose = require('mongoose');

const EmployerSchema = mongoose.Schema({
    name: String,
    email: String,
    password: String
});

const EmployerModel = mongoose.model('employer', EmployerSchema);

module.exports = EmployerModel; // Exportez EmployerModel en utilisant CommonJS
