const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const EmployerModel = require('./models/employer.js');
const bcrypt = require('bcrypt');

const app = express();
app.use(express.json());
app.use(cors());

const PORT = process.env.PORT || 3000;

app.post('/register', async (req, res) => {
    try {
        const { name, email, password } = req.body;
        const hash = await bcrypt.hash(password, 10);
        const employer = await EmployerModel.create({ name, email, password: hash });
        res.json(employer);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});

app.post('/login', async (req, res) => {
    try {
        const { email, password } = req.body;
        const user = await EmployerModel.findOne({ email: email });
        if (user) {
            const match = await bcrypt.compare(password, user.password);
            if (match) {
                res.json("success");
            } else {
                res.json("Password incorrect");
            }
        } else {
            res.json("Enregistrement échoué");
        }
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});

mongoose.connect("mongodb://localhost:27017/employer")
    .then(() => {
        app.listen(PORT, () => console.log(`Connexion réussie sur le port ${PORT}`));
    })
    .catch((err) => console.error("Erreur de connexion à la base de données:", err));
