import { useState } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import axios from 'axios'
import {useNavigate} from 'react-router-dom'

function Signup() {
    const [name,setName] = useState()
    const [email, setEmail]= useState()
    const [password , setPassword]= useState()
    const navigate = useNavigate()

const handleSubmit =(e)=>{
    e.preventDefault()
    axios.post('http://localhost:3000/register',{name,email,password})
    .then(result =>{
      console.log(result)
        navigate('/login')
    })
    .catch(err=>console.log(err)) 
}

  return (
    <Container>
      <Row className="justify-content-md-center mt-5">
        <Col xs={12} md={6}>
          <h2 className="mb-4">Sign Up</h2>
          <Form onSubmit={handleSubmit}>
            <Row className="mb-3">
              <Form.Group as={Col} controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter your first name"
                  name="firstName"
                //   value={formData.firstName}
                  onChange={(e)=>setName(e.target.value)}
                  required
                />
              </Form.Group>
            </Row>

            <Form.Group className="mb-3" controlId="email">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                name="email"
                // value={formData.email}
                onChange={(e)=>setEmail(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                name="password"
                // value={formData.password}
                onChange={(e)=>setPassword(e.target.value)}
                required
              />
            </Form.Group>

            <Button variant="primary" type="submit">
              Sign Up
            </Button>
          </Form>
          <Link to="/login">
            <p>Avez vous deja un compte? Connectez-vous</p>
          </Link>
        </Col>
      </Row>
    </Container>
  );
}

export default Signup;
