import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from "./Home";
import Login from "./Login";
import Signup from "./Signup";
import SideNav from "./SideNav";

function App() {
  return (
    <BrowserRouter>
      <div>
        <SideNav />
        <Routes>
          <Route path="/register" element={<Signup />} />
          <Route path="/home" element={<Home />} />
          <Route path="/login" element={<Login />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
