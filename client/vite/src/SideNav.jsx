import { useState } from 'react';
import { Container, Row, Col, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function SideNav() {
  const [activeTab, setActiveTab] = useState('home');

  const handleTabSelect = (tab) => {
    setActiveTab(tab);
  };

  const navItems = [
    { path: '/home', label: 'Home', key: 'home' },
    { path: '/login', label: 'Login', key: 'login' },
    { path: '/register', label: 'Register', key: 'register' },
  ];

  return (
    <Container>
      <Row>
        <Col sm={3}>
          <Nav className="flex-column">
            {navItems.map(item => (
              <Nav.Item key={item.key}>
                <Nav.Link
                  as={Link}
                  to={item.path}
                  eventKey={item.key}
                  active={activeTab === item.key}
                  onClick={() => handleTabSelect(item.key)}
                >
                  {item.label}
                </Nav.Link>
              </Nav.Item>
            ))}
          </Nav>
        </Col>
        <Col sm={9}>
          {/* Contenu de la page en fonction de l'onglet sélectionné */}
          {/* Les routes correspondantes s'afficheront ici */}
        </Col>
      </Row>
    </Container>
  );
}

export default SideNav;
